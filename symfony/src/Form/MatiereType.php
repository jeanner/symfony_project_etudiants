<?php

namespace App\Form;

use App\Entity\Matiere;
use App\Entity\Intervenant;
use App\Repository\IntervenantRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class MatiereType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('intervenant', EntityType::class, [
                'class' => Intervenant::class,
                'choice_label' => function ($intervenant) {
                    return $intervenant->getNom();
                }
                // ,
                // 'query_builder' => function(IntervenantRepository $int) use ($options) {
                //     $firstRequest = $int->createQueryBuilder('inter')
                //         ->leftJoin('inter.matiere', 'p') 
                //         ->groupBy('inter.id')
                //         ->having('count(p.id) < 2');
                //     $secondRequest = $int->createQueryBuilder('o')
                //         ->where('o.id in (' . $firstRequest->getDQL() . ')')
                //         ->orWhere('o in (:intervenant)')
                //         ->setParameter('intervenant', $options['intervenant']);
                //         return $secondRequest;
                // }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Matiere::class,
        ]);
    }
}
