<?php

namespace App\Form;

use App\Entity\Projet;
use App\Entity\Matiere;
use App\Entity\Etudiant;
use App\Repository\EtudiantRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ProjetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //$etudiant = $options['etudiants'];
        $builder
            ->add('nom')
            ->add('maxEtu')
            ->add('note')
            ->add('etudiants',EntityType::class,[
                'multiple' => true,
                'class' =>Etudiant::class,
                'choice_label' => function ($etudiants){
                    return $etudiants->getNom();
                },
                'query_builder' => function(EtudiantRepository $int) use ($options) {
                    $req = $int->createQueryBuilder('proj')
                        ->leftJoin('proj.projet', 'p') 
                        ->groupBy('proj.id')
                        ->having('count(p.id) < 4');
                    //return $int->createQueryBuilder('a')
                    //     ->andWhere('a.id in (:etudiant)')
                    //     ->orWhere('a.id IN ('.$req->getDQL().')')
                    //     ->setParameter('etudiant', $etudiant);
                        
                }
            ])
            ->add('matiere', EntityType::class, [
                'class' => Matiere::class,
                'required' => false,
                'choice_label' => function ($matiere) {
                    return $matiere->getNom();
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Projet::class,
        ]);
    }
}
